const { Schema, model } = require('mongoose');



const blogsSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    userId: {
        type : Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    date:{
        type: Date,
        required: true
    }
})


blogsSchema.statics.getWeekDay = (blogs)=> {
    let days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
    let counts = {};
    const day = blogs.map(item => days[item.date.getDay()]);
    day.sort()
    day.forEach((x) => { return counts[x] = (counts[x] || 0)+1; })
    return counts;
}

module.exports = model('Blogs', blogsSchema)