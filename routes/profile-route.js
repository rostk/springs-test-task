require('dotenv').config();
const { Router } = require('express');
const router = Router();
const User = require('../models/user-model');
const auth = require('../middleware/auth-middleware');
const permission = require('../middleware/permission.js');
const { validationResult } = require('express-validator');
const { editValidators} = require('../utils/validators');
const bcrypt = require('bcryptjs');

router.put('/', editValidators, auth, permission, async (req, res) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect data'
            })
        }
        if(req.body.password){
            req.body.password = await bcrypt.hash(req.body.password, 12)
        }
        await User.findByIdAndUpdate(req.user.userId, req.body, { new: true, upsert: true });

        res.status(200).json({ message: 'User data has been updated' })
    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' })
    }
})



module.exports = router;