const { Router } = require('express');
const router = Router();
const { validationResult } = require('express-validator');
const auth = require('../middleware/auth-middleware');
const Blogs = require('../models/blogs-model');
const permission = require('../middleware/permission.js');
const { blogValidators } = require('../utils/validators')


router.post('/create', blogValidators, auth, async (req, res) => { //create post with fields (title, description, date, userId(ref to User model))
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array().map(m => m.msg),
                message: 'Incorrect data'
            })
        }
        const { title, description } = req.body;
        const blogs = new Blogs({
            title,
            description,
            userId: req.user.userId,
            date: new Date()
        });
        await blogs.save();
        console.log(blogs)
        res.status(201).json(blogs)
    } catch (error) {
        res.status(500).json({ message: 'Something goes wrong' });
    }
})

router.put('/edit/:id', blogValidators, auth, permission, async (req, res) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect data'
            })
        }
        await Blogs.findByIdAndUpdate(req.params.id, req.body, { new: true, upsert: true });

        res.status(200).json({ message: 'Post data has been updated' })
    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' })
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const blogs = await Blogs.findById(req.params.id);
        res.status(200).json(blogs);
    } catch (e) {
        res.status(500).json(e);
    }
});

router.get('/', auth, async (req, res) => { //sets limit and offset of output blogs by query (http://localhost:3000/blogs?skip=1&limit=5) example
    try {
        console.log("limit :" + req.query.limit + "skip :" + req.query.skip)
        const blogs = await Blogs.find().skip(+req.query.skip).limit(+req.query.limit);
        res.status(200).json(blogs);
    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' });
    }
});

router.get('/stats/days', auth, async (req, res) => { //returns object of days and numer publication of last week by curret user 
    try {

        let blogs = await Blogs.find({
            userId: req.user.userId,
            date: { $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000) }
        })
        blogs = Blogs.getWeekDay(blogs);
        res.status(200).json(blogs);
    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' })
    }
});
router.delete('/delete/:id', auth, permission, async (req, res) => {
    try {
        const blog = await Blogs.findByIdAndDelete(req.params.id);
        res.status(200).json(blog)
    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' })
    }
});

router.get('/search/content', auth, async (req, res) => {  //search content by query like (http://localhost:3000/blogs/search/content?search=)
    try {
        const search = await Blogs.find(
            {
                $or: [      //return blogs whitch includes expression in content(title or description)
                    { title: { "$regex": req.query.search, "$options": "i" } },
                    { description: { "$regex": req.query.search, "$options": "i" } }
                ]
            });
        res.status(200).json(search);
    } catch (error) {
        res.status(500).json({ message: 'Something goes wrong' });
    }
});

module.exports = router;