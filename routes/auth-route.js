require('dotenv').config();

const { Router } = require('express');
const router = Router();
const User = require('../models/user-model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const regEmail = require('../emails/registration');
const nodemailer = require('nodemailer');
const sendgrid = require('nodemailer-sendgrid-transport');
const { registerValidators, loginValidators } = require('../utils/validators');


const transporter = nodemailer.createTransport(sendgrid({ //check SPAM!
    auth: {
        api_key: process.env.SENDGRID_API_KEY
    }
}));


function generateAccessToken(userId) {
    return jwt.sign(userId, process.env.SECRET_KEY, { expiresIn: '1h' })
};

let refreshTokens = [];     //not the best way,after restart server arr will be empty

router.post('/register', registerValidators, async (req, res) => {
    try {
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array().map(m=> m.msg),
                message: 'Incorrect data'
            })
        }

        const { email, password, name } = req.body

        const hashedPassword = await bcrypt.hash(password, 12)
        console.log(hashedPassword)
        const user = new User({
            email,
            name,
            password: hashedPassword
        })
        console.log(user)
        await user.save();

        await transporter.sendMail(regEmail(email));   //check SPAM!
        res.status(201).json({ message: `User created ${user}` })

    } catch (e) {
        res.status(500).json({ message: 'Something goes wrong' })
    }
})


router.post('/login', loginValidators, async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array().map(m=> m.msg),
                    message: 'Incorrect data'
                })
            }

            const { email, password } = req.body
            const user = await User.findOne({ email })
            if (!user) {
                return res.status(400).json({ message: 'User not found' })
            }

            const compare = await bcrypt.compare(password, user.password)
            if (!compare) {
                return res.status(400).json({ message: 'Incorrect password, try again' })
            }

            const accessToken = generateAccessToken({ userId: user._id });

            const refreshToken = jwt.sign({ userId: user._id }, process.env.REFRESH)

            refreshTokens.push(refreshToken); 
            console.log(refreshToken)
            res.json({ accessToken, refreshToken})

        } catch (e) {
            res.status(500).json({ message: 'Something goes wrong, try again' })
        }
    })


router.post('/token', (req, res) => {             //request to generate access token by refresh token from body { "token":"refresh" }
    const refreshToken = req.body.token
    if (refreshToken == null) return res.sendStatus(401).json({ message: 'token required' })
    if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403).json({ message: 'token is not correct' })
    jwt.verify(refreshToken, process.env.REFRESH, (err, user) => {
        if (err) return res.sendStatus(403)
        const accessToken = generateAccessToken({ userId: user._id })
        res.json({ accessToken: accessToken })
    })
})



module.exports = router;

