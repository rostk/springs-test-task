require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');


app.use(express.json({ extended: true }))


app.use('/auth', require('./routes/auth-route'));
app.use('/profile/edit', require('./routes/profile-route'));
app.use('/blogs', require('./routes/blogs-route'));

async function start() {
    try {
      await mongoose.connect(process.env.MONGO_API_KEY, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
      })
      const PORT = 3000;
      app.listen(PORT, () => console.log(`Server listening on port ${PORT}...`))
    } catch (e) {
      console.log('Server Error', e.message)
      process.exit(1)
    }
  }
  
  start()