
const { body } = require('express-validator');
const User = require('../models/user-model');

exports.registerValidators = [
    body('email', 'Email is not correct,please try again').isEmail().custom(async(value, {req})=>{
        try{
            const user = await User.findOne({email : value});
            if(user){
                return Promise.reject(`Email ${value} already exist,please try again`)
            }
        }catch(e){
            console.log(e);
        }
    }),
    body('name', 'Name has to be more than 3 symbols').isLength({min : 3}),
    body('password', 'Password has to be minimum 6 symbols').isLength({min : 6, max : 56})
];

exports.loginValidators = [
    body('email', 'Incorrect email').normalizeEmail().isEmail(),
    body('password', 'Enter password').exists()
]

exports.blogValidators = [
    body('title', 'Incorrect length').isLength({ min: 2, max: 16 }),
    body('description', 'Incorrect length').isLength({ min: 3, max: 254 })
]
exports.editValidators = [
    body('email', 'Email is not correct,please try again').optional().isEmail().custom(async(value, {req})=>{
        try{
            const user = await User.findOne({email : value});
            if(user){
                return Promise.reject(`Email ${value} already exist,please try again`)
            }
        }catch(e){
            console.log(e);
        }
    }),
    body('name', 'Name has to be more than 3 symbols').optional().isLength({min : 3}),
    body('password', 'Password has to be minimum 6 symbols').optional().isLength({min : 6, max : 56})
] 