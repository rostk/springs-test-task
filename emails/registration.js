

module.exports = function(email){
    return { 
        to : email,
        from : 'springs-task-test@gmail.com',
        subject : 'User has been created!',
        html : `<div>Welcome ${email} to out team</div>`
    }

}