const Blogs = require('../models/blogs-model');
module.exports = async (req, res, next) => {
  try {
    const owner = await Blogs.findById(req.params.id)
    if(req.user.userId != owner.userId){
        return res.status(403).json({message: 'Permission denied '})
    }
    next();
  } catch (e) {
    res.status(500).json({message: 'Something goes wrong '})
  }
}